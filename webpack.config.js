var webpack                     =   require('webpack');
var path                        =   require('path');
var CopyWebpackPlugin           =   require('copy-webpack-plugin');
var ExtractTextPlugin           =   require("extract-text-webpack-plugin");
var UglifyJSPlugin              =   require('uglifyjs-webpack-plugin');

module.exports = {
    entry: './app/app.js',
    output: {
        filename: './public/dist/bundle-script.js'
    },
    plugins: [
        new UglifyJSPlugin(),
        new CopyWebpackPlugin([
            { from: 'app/index.html',           to: 'public/index.html' },
            { from: 'app/css',                  to: 'public/css' },
            { from: 'app/dist',                 to: 'public/dist' },
            { from: 'app/font-awesome',         to: 'public/font-awesome' },
            { from: 'app/fonts',                to: 'public/fonts' },
            { from: 'app/img',                  to: 'public/img' },
            { from: 'app/js/modernizr-2.8.3.min.js',to: 'public/js/modernizr-2.8.3.min.js' },
            /*
            { from: '../donate-1',              to: 'public/donate' },
            */
            { from: '../donate-1/index.html',   to: 'public/donate/index.html' },
            { from: '../donate-1/css',          to: 'public/donate/css' },
            { from: '../donate-1/img',          to: 'public/donate/img' },
            { from: '../donate-1/js',           to: 'public/donate/js' },
            { from: '../donate-1/vendor',       to: 'public/donate/vendor' },
        ], {
            ignore: [
                '**/.git/',
            ],
            copyUnmodified: true
        })
    ]
    /*
    module: {
        loaders: [
            { test: /\.css$/, loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader' }) },
			{ test: /\.(ttf|eot|svg|png|gif|jpg|woff)(\?[a-z0-9]+)?$/, loader : 'file-loader' },
        ]
    },
    plugins: [
        new ExtractTextPlugin("./app/bundle-style.css"),
        new CopyWebpackPlugin([
        ])
    ]
        */
}
