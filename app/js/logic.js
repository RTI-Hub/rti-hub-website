
$(document).ready( function() {
    setTimeout(function() {
        $('.navbar-default').stickUp();
    }, 200);

    new WOW().init();

	var videoWaypoint = $('#what-is-rti').waypoint(function(direction) {
        console.log("Load video");
        $("#vimeo-frame").html('<iframe src="https://player.vimeo.com/video/88665027" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
        videoWaypoint[0].disable();
    }, {
        offset: '80%'
	});

});


