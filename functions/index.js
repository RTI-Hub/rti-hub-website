const SENDGRID_TEMPLATE_ID      =   '09719d80-58ed-4744-bd71-de9f7a3e4b1f';

const functions                 =   require('firebase-functions');
const admin                     =   require("firebase-admin");
const request                   =   require('request-promise');
const helper                    =   require('sendgrid').mail;
const moment                    =   require('moment');
const cors                      =   require('cors')({origin: true});
const sg                        =   require('sendgrid')(functions.config().sendgrid.key);
const service_accounts          =   require('./rti-hub-firebase-adminsdk.json');

admin.initializeApp({
    credential                  :   admin.credential.cert(service_accounts),
    //credential                  :   admin.credential.cert(functions.config().fireadmin),
    databaseURL                 :   functions.config().app.admin_database_url
});

const SLACK_WEBHOOK_URL         =   'https://hooks.slack.com/services/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';

/**
 * Handles write to Donation object. If it's a newly created donation object,
 * then it initiates the function `createPaymentRequest()` to create payment URL
 *
 * TODO: Convert this handler into a HTTP Trigger
 */
exports.writeToDonationObject   = 	functions.database.ref('/donations/{donationId}').onWrite(event => {

    //console.log(functions.config());

    var writtenObject           =   event.data.val();

    if (writtenObject) {
        if (writtenObject.transactionStatus === "Initiated") {
            return createPaymentRequest(event);
        }
    }
    
});

/**
 * This function connects with Instamojo and creates a Payment request.
 * Then it updates the same in corresponding donation object, which will then
 * be picked up by Client and Payment flow happens in Client
 */
function createPaymentRequest(event) {

    var writtenObject           =   event.data.val();

	var headers 				= 	{ 'X-Api-Key': functions.config().insta.api_key, 'X-Auth-Token': functions.config().insta.auth_token}
	var payload 				= 	{
	  	purpose					: 	"RTI Date Calculator - Donation",
	  	amount					: 	writtenObject.donation,
	  	phone					: 	writtenObject.mobile,
	  	buyer_name				: 	writtenObject.displayName,
	  	redirect_url			: 	functions.config().insta.redirect_url,
	  	send_email				: 	true,
	  	send_sms				: 	true,
	  	email					: 	writtenObject.email,
        webhook                 :   functions.config().insta.webhook,
	  	allow_repeated_payments	: 	false
	};

    //console.log(writtenObject);
	//console.log("Payload: ", JSON.stringify(payload));

  	return request({
  	  	uri						: 	functions.config().insta.url,
  	  	method					: 	'POST',
        headers                 :   headers,
  	  	form					: 	payload,
  	  	resolveWithFullResponse	: 	true
  	}).then(response => {
        var snapshot            =   event.data;
  	  	if (response.statusCode >= 400) {
  	  	  	//throw new Error(`HTTP Error: ${response.statusCode}`);
            console.log("Error from Instamojo");
            body                    =   JSON.parse(response.body);
            var snapshot            =   event.data;
            var updateData          =   {
                "transactionStatus" :   "ERROR: Invalid Mobile Number"
            };
  	  	} else {
            //console.log('INSTA SUCCESS! Posted', response.body);
            body                    =   JSON.parse(response.body);
            var snapshot            =   event.data;
            var updateData          =   {
                "transactionStatus" :   "Instamojo payment request created",
                "paymentRequestID"  :   body.payment_request.id,
                "paymentURL"        :   body.payment_request.longurl,
            };
        }
        //console.log("Insta Update: ", updateData);
        return snapshot.ref.update(updateData);
  	}, error => {
        //throw new Error(`HTTP Error: ${response.statusCode}`);
        console.log("Error from Instamojo", error);
        //body                    =   JSON.parse(error.body);
        var snapshot            =   event.data;
        var updateData          =   {
            "transactionStatus" :   "ERROR: Invalid Mobile Number"
        };
        return snapshot.ref.update(updateData);
    });

    /*
    // Post to Slack
  	request({
  	  	uri						: 	WEBHOOK_URL,
  	  	method					: 	'POST',
  	  	body					: 	JSON.stringify(payload),
  	  	resolveWithFullResponse	: 	true
  	}).then(response => {
  	  	if (response.statusCode >= 400) {
  	  	  	throw new Error(`HTTP Error: ${response.statusCode}`);
  	  	}
  	  	console.log('SUCCESS! Posted', response);
  	});
    */

};

/**
 * HTTP Trigger to handle successful payment.
 * This is a Webhook for Instamojo to call, once the Payment is success
 * URL: https://us-central1-rti-fundraiser.cloudfunctions.net/handleSuccessfulPayment
 */
exports.handleSuccessfulPayment =   functions.https.onRequest((req, res) => {
    if (req.method === 'PUT') {
        res.status(403).send('Forbidden!');
    }

    var body                    =   req.body;
    var child;

    //console.log(req.body);
    //console.log(req.body.payment_request_id);

    var donationObject          =   admin.database().ref('/donations/').orderByChild("paymentRequestID").equalTo(body.payment_request_id);
    return donationObject.once('value', snapshot => {
        //console.log(snapshot.val());
        snapshot.forEach(childSnapshot => {
            child               =   childSnapshot.val(); 
            return true;
        });
        //console.log(child);

        var donationData    =   {
            transactionStatus:  "Payment Success",
            paymentID       :   body.payment_id
        };  
        var p1              =   sendDonationMail(child);
        var p2              =   admin.database().ref('/donations/' + child.id).update(donationData);

        return Promise.all([p1, p2]);

    }, function(errorObject) {
        console.log('Handle Successful Payment :: Donation Object Retrieve Error: ', errorObject);
        res.end();
    }).then(() => {

        var summaryObject   =   admin.database().ref('/summary');
        return summaryObject.once('value', snapshot => {
            var summary             =   snapshot.val();

            var summaryData         =   {
                amount              :   summary.amount + parseInt(child.donation)
            };  

            return admin.database().ref('/summary').update(summaryData);
        }, function(errorObject) {
            console.log('Handle Successful Payment :: Donation Object Retrieve Error: ', errorObject);
            res.end();
        });


    }).then(() => {
        res.status(200).send("Thanks");
        res.end();
    });

});


/**
 * This function send Thank mailer to Donor via Sendgrid,
 * once the Payment completion is notified via the Webhook
 */
function sendDonationMail(donationObject) {
    return new Promise(function(resolve, reject) {
        var request = sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: {
                personalizations: [
                    {
                        to: [
                            {
                                email: donationObject.email,
                            },
                        ],
                        'substitutions': {
                            '[[donor_name]]': donationObject.displayName,
                            '[[donation_amount]]': donationObject.donation,
                            '[[donation_id]]': donationObject.id,
                            '[[donation_date_time]]': new Date(parseInt(donationObject.createdAt)).toISOString().replace(/T/, ' ').replace(/\..+/, '')
                        }
                    },
                ],
                from: {
                    email: 'team@grassrootsapp.in',
                    name: "Team Grassroots"
                },
                'template_id': SENDGRID_TEMPLATE_ID,
            },
        });

        sg.API(request, function(error, response) {
            if (error) {
                console.log('Sendgrid Error:', error);
                reject();
            }
            resolve();
        });
    });
}
